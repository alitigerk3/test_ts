import * as dotenv from 'dotenv';

dotenv.config();

['PORT'].forEach((element) => {
  if (!Object.keys(process.env).includes(element)) throw new Error('missing required environment variable');
});

export const config = { 
  port: process.env.PORT,
};
