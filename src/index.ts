import * as express from 'express';
import * as cors from 'cors';
import helmet from 'helmet';

import { config } from './config';
const app: express.Application = express();

app.use(helmet());
app.use(cors());
app.use(express.json());

app.get('/', (req, res)=>{
  res.send('Hello World!')
});

app.listen(config.port, () => {
  console.log('Server run on port', config.port);
});